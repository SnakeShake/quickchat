package com.stivayou.quickchat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private final static String PREFKEY_LAST_USER = "pk_last_user";

    EditText user;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user = findViewById(R.id.main_user);

        prefs = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
        if( prefs.contains(PREFKEY_LAST_USER) )
            user.setText(prefs.getString(PREFKEY_LAST_USER,""));

        findViewById(R.id.main_apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( user.getText().toString().length() < 4 ){
                    Toast.makeText( MainActivity.this, R.string.main_warning_length, Toast.LENGTH_SHORT ).show();
                }
                else{
                    prefs.edit().putString(PREFKEY_LAST_USER, user.getText().toString()).apply();

                    Intent intent = new Intent( MainActivity.this, ChatActivity.class );
                    intent.putExtra(ChatActivity.ARG_USER,user.getText().toString());
                    startActivity( intent );
                }

            }
        });

    }
}
