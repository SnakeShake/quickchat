package com.stivayou.quickchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ChatActivity extends AppCompatActivity {

    public static final String ARG_USER = "arg_user";

    String user = "";

    RecyclerView recycler;
    ChatAdapter adapter;
    EditText text;

    DatabaseReference rawBubbles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        if( getIntent().getExtras() != null )
            user = getIntent().getExtras().getString( ARG_USER, "" );

        recycler = findViewById(R.id.chat_recycler);
        recycler.setLayoutManager( new LinearLayoutManager(this));

        text = findViewById(R.id.chat_text);

        adapter = new ChatAdapter( user );

        recycler.setAdapter(adapter);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        rawBubbles = database.getReference("messages");

        findViewById(R.id.chat_chat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( text.getText().length() < 5 ){
                    Toast.makeText( ChatActivity.this, R.string.chat_warning_length, Toast.LENGTH_SHORT ).show();
                }
                else{

                    ChatBubble bubble = new ChatBubble();
                    bubble.user = user;
                    bubble.message = text.getText().toString();

                    rawBubbles.push().setValue(bubble);
                    text.setText("");
                }

            }
        });

        rawBubbles.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.i("ChatActivity","Child was added");
                ChatBubble bubble = dataSnapshot.getValue(ChatBubble.class);
                adapter.bubbles.add( bubble );
                adapter.notifyItemInserted( adapter.bubbles.size() - 1 );
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }



}
