package com.stivayou.quickchat;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter {

    static final int TYPE_OTHER = 0;
    static final int TYPE_MINE = 1;

    public List<ChatBubble> bubbles = new ArrayList<>();
    String currUser;

    public ChatAdapter( String currUser ){
        this.currUser = currUser;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        Log.i("ChatAdapter","Creating viewholder");

        if( viewType == TYPE_OTHER )
            return new ChatViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.bubble_other,parent,false));
        else
            return new ChatViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.bubble_mine,parent,false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ChatViewHolder cHolder = (ChatViewHolder) holder;

        cHolder.user.setText(bubbles.get(position).user);
        cHolder.message.setText(bubbles.get(position).message);

    }

    @Override
    public int getItemCount() {
        return bubbles.size();
    }

    @Override
    public int getItemViewType(int position) {

        if( bubbles.get(position).user.equals(currUser) )
            return TYPE_MINE;

        return TYPE_OTHER;
    }

    class ChatViewHolder extends RecyclerView.ViewHolder{

        TextView user, message;

        public ChatViewHolder(View itemView) {
            super(itemView);

            user = itemView.findViewById(R.id.bubble_user);
            message = itemView.findViewById(R.id.bubble_message);
        }
    }
}
